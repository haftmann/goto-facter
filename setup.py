
from distutils.core import setup

from debian.changelog import Changelog

with open('debian/changelog', 'rb') as reader:
    chlog = Changelog(reader, max_blocks = 1)
version = chlog.get_version().full_version

description = '''Augmentation layer for facter.'''
long_description = '''Augmentation layer for facter: python interface,
slot for custom facts, query tool for boolean facts.'''


settings = dict(

    name = 'goto-facter',
    version = version,

    packages = ['facter'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@muenchen.de',
    description = description,
    long_description = long_description,

    license = 'EUPL',
    url = 'http://www.muenchen.de'

)

setup(**settings)
