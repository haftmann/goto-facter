
module Limuxlib

  def Limuxlib.first_match_from_file(loc, regexp)

    value = nil
    if File.readable?(loc)
      File::open(loc, "r").each do |line|
        match = regexp.match(line)
        if match
          value = match[1]
          break
        end
      end
    end
    value

  end

end
