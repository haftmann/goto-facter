
'''
    Obtaining facts from facter.
'''


__all__ = ['obtain', 'obtain_all']


import json

from mopynaco import subproc


def invoke(facts):

    facts_txt, _, exit_value = subproc.capture('facter', ['--json', '--'] + list(facts))
    if exit_value != 0:
        raise EnvironmentError('Unable to obtain facts ' + ', '.join(facts))
    return json.loads(facts_txt)


def obtain(*facts):

    data = invoke(facts)
    return [data[fact] for fact in facts]


def obtain_all():

    return invoke([])
