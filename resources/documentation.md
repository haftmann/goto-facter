
# Purpose

Package *goto-facter* provides enhancements over basic *facter*,
particularly:

* A python interface to facter.

* A slot for additional custom facts.

* A small library with ruby utilities.

* A query tool for boolean facts.
